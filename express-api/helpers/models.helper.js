module.exports = {
    paginate: async (models, body, options) => {
        try {
            let data = {
                totalRow: '',
                data: ''
            }
            await models.count()
                .then(res => {
                    return data.totalRow = res
                })
            await models.findAll({
                ...options,
                limit: parseInt(body.pageSize),
                offset: parseInt(body.pageSize * (body.pageIndex - 1))
            })
                .then(res => {
                    return data.data = res.map((i, k) => {
                        i.dataValues.rowNum = parseInt(body.pageSize * (body.pageIndex - 1)) + k + 1
                        return i;
                    })
                })
            return await data;
        } catch (e) {
            throw e
        }
    },
    findAndDelete: (models, options) => {
        return models.findOne({
            ...options
        }).then(response => {
            if (!response) return;
            return models.destroy({
                ...options
            })
        })
    },
    handlerSuccess: data => {
        if (Array.isArray(data)) {
            return {
                success: true,
                data: data
            }
        }
        if (typeof data === 'object') {
            return {
                success: true,
                ...data
            }
        }
        return {
            success: true,
            message: data
        }
    },
    handlerError: err => {
        return {
            success: false,
            data: null,
            messages: err
        }
    },
}
