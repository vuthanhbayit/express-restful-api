const bcrypt = require('bcrypt')

module.exports.hash = (text) => {
    return bcrypt.hashSync(text, 10)
}

module.exports.compare = (password, confirmPassword) => {
    return bcrypt.compareSync(password, confirmPassword)
}
