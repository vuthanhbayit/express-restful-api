const {permissions} = require('../models');
const {paginate, handlerSuccess, handlerError} = require('../helpers/models.helper');
const {like} = require('sequelize').Op;

module.exports = {
    search: (req, res) => {
        const options = {
            where: {
                fullname: {
                    [like]: `%${req.body.keyword}%`
                }
            }
        }
        return paginate(permissions, req.body, options)
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err.errors)))
            })
    },
    update: (req, res) => {
        let path = 'create';
        let options = {};
        let message = 'CreatePermissionSuccess'
        if (req.body.id > 0) {
            path = 'update';
            message = 'UpdatePermissionSuccess'
            options = {
                where: {
                    id: req.body.id
                }
            }
        } else {
            delete req.body.id
        }
        return permissions[path](req.body, {
            ...options
        })
            .then(response => {
                res.json((handlerSuccess({
                    data: response,
                    message: message
                })))
            })
            .catch(err => {
                res.json((handlerError(err.errors)))
            })
    },
    detail: (req, res) => {
        return permissions.findByPk(req.body.id)
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err.errors)))
            })
    },
    delete: (body) => {

    }
}
