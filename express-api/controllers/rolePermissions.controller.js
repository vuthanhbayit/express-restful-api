const {rolePermissions} = require('../models');
const {paginate, findAndDelete, handlerSuccess, handlerError} = require('../helpers/models.helper');
const {like} = require('sequelize').Op;

module.exports = {
    search: (req, res) => {
        return paginate(rolePermissions, req.body)
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err)))
            })
    },
    update: (req, res) => {
        const options = {
            where: {
                roleId: req.body.roleId
            }
        }
        return findAndDelete(rolePermissions, options)
            .then(() => {
                let data = req.body.permissionId.map(i => {
                    return {
                        roleId: req.body.roleId,
                        permissionId: i
                    }
                })
                rolePermissions.bulkCreate(data)
                    .then(response => {
                        res.json(handlerSuccess(response))
                    })
            })
    },
    detail: (req, res) => {
        return permissions.findByPk(req.body.id)
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err)))
            })
    },
}
