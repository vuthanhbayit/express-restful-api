const {users} = require('../models');
const {like, or} = require('sequelize').Op;
const {paginate, handlerSuccess, handlerError} = require('../helpers/models.helper');
const {hash, compare} = require('../helpers/bcrypt.helpers');


module.exports = {
    login: async (req, res) => {
        let user = await users.findOne({
            where: {
                email: req.body.email
            }
        })
        if (!user) res.json(handlerError("Email don't exist"))
        if (!compare(req.body.password, user.dataValues.password)) res.json(handlerError('Email or password is incorrect'))
        res.json(handlerSuccess(user));
    }
}
