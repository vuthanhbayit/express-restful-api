const {users} = require('../models');
const {like, or} = require('sequelize').Op;
const {paginate, handlerSuccess, handlerError} = require('../helpers/models.helper');
const {hash, compare} = require('../helpers/bcrypt.helpers');


module.exports = {
    search: (req, res) => {
        const options = {
            attributes: ['id', 'avatar', 'firstName', 'lastName', 'email'],
            where: {
                [or]: [
                    {
                        firstName: {
                            [like]: `%${req.body.keyword}%`
                        }
                    },
                    {
                        lastName: {
                            [like]: `%${req.body.keyword}%`
                        }
                    },

                ]
            }
        }
        return paginate(users, req.body, options)
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err)))
            })
    },
    update: (req, res) => {
        let path = 'create';
        let message = 'CreateUserSuccess'
        let options = {}
        if (req.body.id > 0) {
            path = 'update';
            message = 'UpdateUserSuccess'
            options = {
                where: {
                    id: req.body.id
                }
            }
        } else {
            delete req.body.id
        }
        req.body.password = hash(req.body.password)
        return users[path](req.body, {
            ...options
        })
            .then(response => {
                res.json((handlerSuccess({
                    data: response,
                    message: message
                })))
            })
            .catch(err => {
                res.json((handlerError(err)))
            })
    },
    detail: (req, res) => {
        return users.findByPk(req.body.id, {
            attributes: ['id', 'username'],
            include: [{
                model: tasks,
                attributes: ['id', 'title']
            }]
        })
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err)))
            })
    },
    login: async (req, res) => {
        let user = await users.findOne({
            where: {
                email: req.body.email
            }
        })
        if (!user) res.json(handlerError("Email don't exist"))
        if (!compare(req.body.password, user.dataValues.password)) res.json(handlerError('Email or password is incorrect'))

    }
}
