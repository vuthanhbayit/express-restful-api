const {roles, permissions} = require('../models');
const {paginate, handlerSuccess, handlerError} = require('../helpers/models.helper');
const {like} = require('sequelize').Op;

module.exports = {
    search: (req, res) => {
        const options = {
            attributes: ['id', 'name', 'fullname', 'description', 'status'],
            where: {
                fullname: {
                    [like]: `%${req.body.keyword}%`
                }
            },
            include: ['permissions']
        }
        return paginate(roles, req.body, options)
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err.errors)))
            })
    },
    update: (req, res) => {
        let path = 'create';
        let message = 'CreateRoleSuccess'
        let options = {}
        if (req.body.id > 0) {
            path = 'update';
            message = 'UpdateRoleSuccess'
            options = {
                where: {
                    id: req.body.id
                }
            }
        } else {
            delete req.body.id;
        }
        return roles[path](req.body, {
            ...options
        })
            .then(response => {
                res.json((handlerSuccess({
                    data: response,
                    message: message
                })))
            })
            .catch(err => {
                res.json((handlerError(err.errors)))
            })
    },
    detail: (req, res) => {
        return roles.findByPk(req.body.id, {
            include: ['permissions']
        })
            .then(response => {
                res.json((handlerSuccess(response)))
            })
            .catch(err => {
                res.json((handlerError(err.errors)))
            })
    },
    delete: (body) => {

    }
}
