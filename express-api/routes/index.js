const express = require('express')
const router = express.Router()

//api users
router.use('/', require('./auth.route'))
router.use('/users', require('./users.route'))
router.use('/roles', require('./roles.route'))
router.use('/permissions', require('./permissions.route'))
router.use('/rolePermissions', require('./rolePermissions.route'))

module.exports = router
