const {Router} = require('express')
const router = Router()
const {search, update, detail} = require('../controllers/permissions.controller')

router.post('/search', search);
router.post('/update', update);
router.post('/detail', detail);

module.exports = router
