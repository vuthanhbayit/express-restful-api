require('dotenv').config()
const app = require('./app');
const server = require('http').createServer(app);

const port = process.env.PORT || 3000;
const host = process.env.HOST || '127.0.0.1';

server.listen(port, host)
console.log('Open server: http://%s:%s', host, port);

socketStart(server)
function socketStart(server) {
    const io = require('socket.io').listen(server)
    io.on('connection', socket => {
        console.log('id: ' + socket.id + ' is connected')

        require('./socket.io').run(io, socket)

        socket.on('disconnect', () => {
            console.log('id: ' + socket.id + ' is disconnected')
        })
    })
}

