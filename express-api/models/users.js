'use strict';
module.exports = (sequelize, DataTypes) => {
    var User = sequelize.define('users', {
        avatar: DataTypes.STRING,
        firstName: DataTypes.STRING,
        lastName: DataTypes.STRING,
        username: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false,
            validate: {
                isEmail: true
            }
        },
        password: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        instanceMethods: {
            compare: function(password) {
                console.log(password)
                console.log(this.password)
            }
        }
    });

    User.associate = models => {
        models.users.belongsTo(models.roles, {
            foreignKey: {
                allowNull: false
            }
        });
    };

    return User;
};
