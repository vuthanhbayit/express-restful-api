'use strict';
module.exports = (sequelize, DataTypes) => {
    const Role = sequelize.define('roles', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        fullname: DataTypes.STRING,
        description: DataTypes.STRING,
        status  : {
            type: DataTypes.STRING,
            defaultValue: 1,
        },
    });

    Role.associate = models => {
        models.roles.belongsToMany(models.permissions, {
            as: 'permissions',
            through: models.rolePermissions,
        });
        models.roles.hasMany(models.users);
    };

    return Role;
};
