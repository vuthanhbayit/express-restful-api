'use strict';
module.exports = (sequelize, DataTypes) => {
    const RolePermissions = sequelize.define('rolePermissions', {
        roleId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                notEmpty: true,
            }
        },
        permissionId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            validate: {
                notEmpty: true,
            }
        }
    });

    RolePermissions.associate = models => {
        models.rolePermissions.belongsTo(models.roles, {foreignKey: 'roleId'});
        models.rolePermissions.belongsTo(models.permissions, {foreignKey: 'permissionId'});
    };

    return RolePermissions;
};
