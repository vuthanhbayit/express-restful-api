'use strict';
module.exports = (sequelize, DataTypes) => {
    const Permission = sequelize.define('permissions', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            unique: true
        },
        fullname: DataTypes.STRING,
        description: DataTypes.STRING,
        status  : {
            type: DataTypes.STRING,
            defaultValue: 1,
        },
    });

    Permission.associate = models => {
        models.permissions.belongsToMany(models.roles, {
            as: 'roles',
            through: models.rolePermissions,
        });
    };

    return Permission;
};
