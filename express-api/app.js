const express = require('express');
const logger = require('morgan');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Require API routes
const routes = require('./routes')
app.use('/api', routes)

module.exports = app;
